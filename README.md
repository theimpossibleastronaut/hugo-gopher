Reads a flat JSON file and makes it publishable as a gophermenu.
The configuration options speak for themselves.
Copy configuration.example to configuration.json.

Just a quick script. It should be better at some point.

TODO
- add directories
- add sorting

If you use [Hugo](https://gohugo.io) as my intended purpose for this script,
add JSON to your TOML configuration.

```
[outputs]
	home = ["HTML","RSS","JSON"]
```

And create a JSON output in your theme folder called index.json:
```
{{- $.Scratch.Add "index" slice -}}
{{- range .Site.RegularPages -}}
    {{- $.Scratch.Add "index" (dict "title" .Title "body" .Plain "id" .Permalink) -}}
{{- end -}}
{{- $.Scratch.Get "index" | jsonify -}}
```

The json format should be like this:
```
[
	{
		"body": "Textual contents of item 1",
		"title": "Item 1",
		"uri": "/items/1"
	},
	{
		"body": "Textual contents of item 2",
		"title": "Item 2",
		"uri": "/items/2"
	},
]
```

This removes a bit of formatting too agressively but alas.

LICENSE;
Just give some attribution and do what you want with this.

Also let me know @sexybiggetje@mastodon.social if you want me to check it out.
